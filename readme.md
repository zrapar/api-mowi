## Installation

- Edit `.env` and set your database connection details
- Run `php artisan key:generate` and `php artisan jwt:secret`
- Run `php artisan migrate:fresh --seed`
