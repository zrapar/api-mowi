<?php

use App\Http\Resources\User as UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('api')->get('/', function () {
  return [
    'app' => config('app.name'),
    'version' => '1.0.0',
  ];
});

Route::group(['prefix' => 'api', 'middleware' => 'auth:api'], function () {
  Route::get('auth/me', function (Request $request) {
    return new UserResource($request->user());
  });

  Route::patch('account/profile', 'Account\ProfileController@update');
  Route::patch('account/password', 'Account\PasswordController@update');
});

Route::group(['prefix' => 'api', 'middleware' => 'guest:api'], function () {
  Route::post('auth/login', 'Auth\LoginController@login');
  Route::post('auth/register', 'Auth\RegisterController@register');
  Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('password/reset', 'Auth\ResetPasswordController@reset');
});

Route::group(['prefix' => 'api/brands', 'middleware' => 'auth:api'], function () {
  Route::get('/', 'API\BrandController@getAll');
  Route::post('/', 'API\BrandController@store');
  Route::get('/{uuid}', 'API\BrandController@show');
  Route::post('/{uuid}', 'API\BrandController@update');
  Route::delete('/{uuid}', 'API\BrandController@destroy');
});

Route::group(['prefix' => 'api/categories', 'middleware' => 'auth:api'], function () {
  Route::get('/', 'API\CategoryController@getAll');
  Route::post('/', 'API\CategoryController@store');
  Route::get('/{uuid}', 'API\CategoryController@show');
  Route::post('/{uuid}', 'API\CategoryController@update');
  Route::delete('/{uuid}', 'API\CategoryController@destroy');
});

Route::group(['prefix' => 'api/linelists', 'middleware' => 'auth:api'], function () {
  Route::get('/', 'API\LineListController@getAll');
  Route::post('/', 'API\LineListController@store');
  Route::get('/{uuid}', 'API\LineListController@show');
  Route::post('/{uuid}', 'API\LineListController@update');
  Route::delete('/{uuid}', 'API\LineListController@destroy');
});

Route::group(['prefix' => 'api/presentation', 'middleware' => 'auth:api'], function () {
  Route::get('/', 'API\PresentationController@getAll');
  Route::post('/', 'API\PresentationController@store');
  Route::get('/{uuid}', 'API\PresentationController@show');
  Route::post('/{uuid}', 'API\PresentationController@update');
  Route::delete('/{uuid}', 'API\PresentationController@destroy');
});

Route::group(['prefix' => 'api/marketing', 'middleware' => 'auth:api'], function () {
  Route::get('/', 'API\MarketingController@getAll');
  Route::post('/', 'API\MarketingController@store');
  Route::get('/{uuid}', 'API\MarketingController@show');
  Route::post('/{uuid}', 'API\MarketingController@update');
  Route::delete('/{uuid}', 'API\MarketingController@destroy');
});

Route::group(['prefix' => 'api/products', 'middleware' => 'auth:api'], function () {
  Route::get('/', 'API\ProductController@getAll');
  Route::post('/', 'API\ProductController@store');
  Route::get('/{uuid}', 'API\ProductController@show');
  Route::post('/{uuid}', 'API\ProductController@update');
  Route::delete('/{uuid}', 'API\ProductController@destroy');
});

Route::group(['prefix' => 'api/videos', 'middleware' => 'auth:api'], function () {
  Route::get('/', 'API\VideoController@getAll');
  Route::post('/', 'API\VideoController@store');
  Route::get('/{uuid}', 'API\VideoController@show');
  Route::post('/{uuid}', 'API\VideoController@update');
  Route::delete('/{uuid}', 'API\VideoController@destroy');
});