<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarketingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketings', function (Blueprint $table) {
            $table->uuid('uuid')->unique()->primary();
            $table->uuid('brand_uuid');
            $table->string('name');
            $table->enum('type', ['consumer', 'foods']);
            $table->longText('documents');
            $table->longText('videos')->nullable();
            $table->timestamps();
            $table->foreign('brand_uuid')->references('uuid')->on('brands')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketings');
    }
}