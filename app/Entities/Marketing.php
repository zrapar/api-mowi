<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUUID;
use App\Traits\UUIDIsPrimaryKey;

class Marketing extends Model
{
    use HasUUID, UUIDIsPrimaryKey;

    protected $primaryKey = 'uuid';

    protected $table = 'marketings';

    protected $fillable = [
        'uuid',
        'name',
        'type',
        'brand_uuid',
        'documents',
        'videos',
    ];

    public function brands()
    {
        return $this->belongsTo(Brands::class, 'brand_uuid', 'uuid')->orderByDesc('created_at');
    }

    public static function getDocumentsAttribute($documents)
    {
        return json_decode($documents);
    }

    public static function getVideosAttribute($videos)
    {
        if (isset($videos)) {
            return json_decode($videos);
        }
        return null;
    }

    public function setDocumentsAttribute($documents)
    {
        $this->attributes['documents'] = json_encode($documents);
    }

    public function setVideosAttribute($videos)
    {
        if (isset($videos)) {
            $this->attributes['videos'] = json_encode($videos);
        }
    }
}