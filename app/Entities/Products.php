<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUUID;
use App\Traits\UUIDIsPrimaryKey;

class Products extends Model
{
    use HasUUID, UUIDIsPrimaryKey;

    protected $primaryKey = 'uuid';

    protected $table = 'products';

    protected $fillable = [
        'uuid',
        'brand_uuid',
        'name',
        'type',
        'documents',
        'videos',
    ];

    public function categories()
    {
        return $this->hasMany(Category::class, 'product_uuid', 'uuid')->orderByDesc('created_at');
    }

    public function brands()
    {
        return $this->belongsTo(Brands::class, 'brand_uuid', 'uuid')->orderByDesc('created_at');
    }

    public function delete()
    {
        $this->categories()->delete();

        return parent::delete();
    }

    public static function getDocumentsAttribute($documents)
    {
        return json_decode($documents);
    }

    public static function getVideosAttribute($videos)
    {
        if (isset($videos)) {
            return json_decode($videos);
        }
        return null;
    }

    public function setDocumentsAttribute($documents)
    {
        $this->attributes['documents'] = json_encode($documents);
    }

    public function setVideosAttribute($videos)
    {
        if (isset($videos)) {
            $this->attributes['videos'] = json_encode($videos);
        }
    }
}