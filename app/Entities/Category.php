<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUUID;
use App\Traits\UUIDIsPrimaryKey;

class Category extends Model
{
    use HasUUID, UUIDIsPrimaryKey;

    protected $primaryKey = 'uuid';

    protected $table = 'categories';

    protected $fillable = [
        'uuid',
        'name',
        'product_uuid'
    ];

    public function products()
    {
        return $this->belongsTo(Products::class, 'product_uuid', 'uuid')->orderByDesc('created_at');
    }
}