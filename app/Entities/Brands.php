<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUUID;
use App\Traits\UUIDIsPrimaryKey;

class Brands extends Model
{
    use HasUUID, UUIDIsPrimaryKey;

    protected $primaryKey = 'uuid';

    protected $table = 'brands';

    protected $fillable = [
        'uuid',
        'name',
        'type',
        'color',
        'image',
        'background',
    ];

    public function linelists()
    {
        return $this->hasMany(LineList::class, 'brand_uuid', 'uuid')->orderByDesc('created_at');
    }

    public function marketings()
    {
        return $this->hasMany(Marketing::class, 'brand_uuid', 'uuid')->orderByDesc('created_at');
    }

    public function presentations()
    {
        return $this->hasMany(Presentation::class, 'brand_uuid', 'uuid')->orderByDesc('created_at');
    }

    public function products()
    {
        return $this->hasMany(Products::class, 'brand_uuid', 'uuid')->orderByDesc('created_at');
    }

    public function videos()
    {
        return $this->hasMany(Video::class, 'brand_uuid', 'uuid')->orderByDesc('created_at');
    }

    public function delete()
    {

        $this->linelists()->delete();
        $this->marketings()->delete();
        $this->presentations()->delete();
        $this->products()->delete();
        $this->videos()->delete();

        return parent::delete();
    }
}