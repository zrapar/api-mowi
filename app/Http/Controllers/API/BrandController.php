<?php

namespace App\Http\Controllers\API;

use App\Entities\Brands;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;


class BrandController extends Controller
{
    protected $storage;

    public function __construct()
    {
        $this->storage = Storage::disk('images');
        $this->docStorage = Storage::disk('documents');
        $this->videoStorage = Storage::disk('videos');
    }

    public function getAll()
    {
        $brands = Brands::with([
            'linelists',
            'marketings',
            'presentations',
            'products.categories',
            'videos',
        ])->get();

        return response()->json(json_encode([
            'success' => true,
            'data' => $brands
        ]));
    }

    public function store(Request $request)
    {

        try {
            $rules = [
                'name' => 'required|unique:brands,name',
                'color' => 'required|regex:/^#[0-9a-f]/',
                'image' => 'required|image',
                'background' => 'required|image',
            ];

            $messages = [
                'name.required'  => 'The field name is required',
                'name.unique'    => 'Exist another brand with the same name',
                'color.required' => 'The field color is required',
                'color.regex'    => 'This its not a valid color',
                'image.required' => 'The field images is required',
                'image.image' => 'This ist not a valid image',
                'background.required' => 'The field images is required',
                'background.image' => 'This ist not a valid image',
            ];

            $validation = Validator::make($request->all(), $rules, $messages);

            if ($validation->fails()) {
                return response()->json(json_encode([
                    'success' => false,
                    'messages' => $validation->errors()
                ]), 400);
            }

            $data = $request->all();

            $pathImage = $this->storage->putFile('', $data['image']);
            $urlImage = $this->storage->url($pathImage);

            $pathBackground = $this->storage->putFile('', $data['background']);
            $urlBackground = $this->storage->url($pathBackground);

            $newBrand = Brands::create([
                'name' => $data['name'],
                'color' => $data['color'],
                'image' => $urlImage,
                'background' => $urlBackground
            ]);

            return response()->json(json_encode([
                'success' => true,
                'data' => $newBrand,
                'message' => 'Brand Succesfully created'
            ]));
        } catch (\Exception $e) {
            return response()->json(json_encode(
                [
                    'success' => false,
                    'message' => $e->getMessage()
                ],
                500
            ));
        }
    }

    public function show($uuid)
    {
        try {
            $brand = Brands::with(
                'linelists',
                'marketings',
                'presentations',
                'products',
                'products.categories',
                'videos'
            )->find($uuid);
            if (!$brand) {
                return response()->json(json_encode([
                    'success' => false,
                    'message' => 'This Brand doesnt exists'
                ]), 404);
            }

            return response()->json(json_encode(['success' => true, 'data' => $brand]));
        } catch (\Exception $e) {
            return response()->json(
                json_encode(
                    [
                        'success' => false,
                        'message' => $e->getMessage()
                    ]
                ),
                500
            );
        }
    }

    public function update(Request $request)
    {
        try {
            $data = $request->all();

            $rules = [
                'name' => ['required'],
                'color' => 'required|regex:/^#[0-9a-f]/',
                'image' => 'required',
            ];

            $messages = [
                'name.required'  => 'The field name is required',
                'name.unique'    => 'Exist another brand with the same name',
                'color.required' => 'The field color is required',
                'color.regex'    => 'This its not a valid color',
                'image.required' => 'The field images is required',
            ];

            $validation = Validator::make($data, $rules, $messages);

            if ($validation->fails()) {
                return response()->json(json_encode([
                    'success' => false,
                    'messages' => $validation->errors()
                ]), 400);
            }

            $brand = Brands::find($request->uuid);

            if (!$brand) {
                return response()->json(json_encode([
                    'success' => false,
                    'message' => 'This Brand doesnt exists'
                ]), 404);
            }

            $dataUpdate = [
                'name' => $data['name'],
                'color' => $data['color'],
                'image' => $data['image'],
                'background' => $data['background']
            ];

            if (is_file($data['image'])) {
                $filePath = str_replace($this->storage->url('/'), '', $brand->image);

                if ($this->storage->exists($filePath)) {
                    $this->storage->delete($filePath);
                }

                $path = $this->storage->putFile('', $data['image']);
                $url = $this->storage->url($path);
                $dataUpdate['image'] = $url;
            }

            if (is_file($data['background'])) {
                $filePath = str_replace($this->storage->url('/'), '', $brand->background);

                if ($this->storage->exists($filePath)) {
                    $this->storage->delete($filePath);
                }

                $path = $this->storage->putFile('', $data['background']);
                $url = $this->storage->url($path);
                $dataUpdate['background'] = $url;
            }

            $brand->update($dataUpdate);

            return response()->json(json_encode([
                'success' => true,
                'data' => $brand,
                'message' => 'Brand succesfully updated'
            ]));
        } catch (\Exception $e) {
            return response()->json(json_encode(
                [
                    'success' => false,
                    'message' => $e->getMessage()
                ],
                500
            ));
        }
    }

    public function destroy($uuid)
    {
        try {

            $brand = Brands::with(
                'linelists',
                'marketings',
                'presentations',
                'products',
                'videos'
            )->find($uuid);

            if (!$brand) {
                return response()->json(json_encode([
                    'success' => false,
                    'message' => 'This Brand doesnt exists'
                ]), 404);
            }

            if (count($brand['linelists']) > 0) {
                foreach ($brand['linelists'] as $key => $data) {
                    $docs = json_decode($data['documents']);

                    foreach ($docs as $key => $doc) {
                        $filePath = str_replace($this->docStorage->url('/'), '', $doc);

                        if ($this->docStorage->exists($filePath)) {
                            $this->docStorage->delete($filePath);
                        }
                    }
                }
            }
            if (count($brand['marketings']) > 0) {
                foreach ($brand['marketings'] as $key => $data) {
                    $docs = json_decode($data['documents']);

                    foreach ($docs as $key => $doc) {
                        $filePath = str_replace($this->docStorage->url('/'), '', $doc);

                        if ($this->docStorage->exists($filePath)) {
                            $this->docStorage->delete($filePath);
                        }
                    }
                }
            }
            if (count($brand['presentations']) > 0) {
                foreach ($brand['presentations'] as $key => $data) {
                    $docs = json_decode($data['documents']);

                    foreach ($docs as $key => $doc) {
                        $filePath = str_replace($this->docStorage->url('/'), '', $doc);

                        if ($this->docStorage->exists($filePath)) {
                            $this->docStorage->delete($filePath);
                        }
                    }
                }
            }
            if (count($brand['products']) > 0) {
                foreach ($brand['products'] as $key => $data) {
                    $docs = json_decode($data['documents']);

                    foreach ($docs as $key => $doc) {
                        $filePath = str_replace($this->docStorage->url('/'), '', $doc);

                        if ($this->docStorage->exists($filePath)) {
                            $this->docStorage->delete($filePath);
                        }
                    }
                }
            }
            if (count($brand['videos']) > 0) {
                foreach ($brand['videos'] as $key => $data) {
                    $docs = json_decode($data['documents']);

                    foreach ($docs as $key => $doc) {
                        $filePath = str_replace($this->docStorage->url('/'), '', $doc);

                        if ($this->docStorage->exists($filePath)) {
                            $this->docStorage->delete($filePath);
                        }
                    }

                    $vids = json_decode($data['videos']);

                    foreach ($vids as $key => $doc) {
                        $filePath = str_replace($this->videoStorage->url('/'), '', $doc);

                        if ($this->videoStorage->exists($filePath)) {
                            $this->videoStorage->delete($filePath);
                        }
                    }
                }
            }


            $filePathI = str_replace($this->storage->url('/'), '', $brand->image);

            if ($this->storage->exists($filePathI)) {
                $this->storage->delete($filePathI);
            }

            $filePathB = str_replace($this->storage->url('/'), '', $brand->background);

            if ($this->storage->exists($filePathB)) {
                $this->storage->delete($filePathB);
            }


            $brand->delete();

            return response()->json(json_encode([
                'success' => true,
                'message' => 'Brand successfully deleted'
            ]));
        } catch (\Exception $e) {
            return response()->json(json_encode(
                [
                    'success' => false,
                    'message' => $e->getMessage()
                ],
                500
            ));
        }
    }
}