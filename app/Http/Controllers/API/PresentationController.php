<?php

namespace App\Http\Controllers\API;

use App\Entities\Brands;
use Illuminate\Http\Request;
use App\Entities\Presentation;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PresentationController extends Controller
{
    protected $docStorage;
    protected $videoStorage;

    public function __construct()
    {
        $this->docStorage = Storage::disk('documents');
        $this->videoStorage = Storage::disk('videos');
    }

    public function getAll(Request $request)
    {
        $querys = $request->query();

        if (count($querys) > 0) {

            $presentation = Presentation::with('brands')->where($querys)->get()->toArray();

            return response()->json(json_encode([
                'success' => true,
                'data' => $presentation
            ]));
        }

        $presentation = Presentation::with('brands')->get()->toArray();

        return response()->json(json_encode([
            'success' => true,
            'data' => $presentation
        ]));
    }

    public function store(Request $request)
    {
        try {
            $rules = [
                'name' => 'required',
                'type'  => 'required|in:consumer,foods',
                'brand_uuid' => 'required',
                'documents.*' => 'required|file|mimes:pdf',
                'videos.*' => 'file|mimetypes:video/3gpp,video/h264,video/mp4,video/quicktime',
            ];

            $messages = [
                'name.required'  => 'The field name is required',
                'brand_uuid.required'  => 'The field brand is required',
                'documents.*.required' => 'The field document is required',
                'type.required'  => 'The field type is required',
                'type.in'        => 'The type can be only consumer or foods',
            ];

            $validation = Validator::make($request->all(), $rules, $messages);

            if ($validation->fails()) {
                return response()->json(json_encode([
                    'success' => false,
                    'messages' => $validation->errors()
                ]), 400);
            }

            $data = $request->all();

            $brand = Brands::find($data['brand_uuid']);

            if (!$brand) {
                return response()->json(json_encode([
                    'success' => false,
                    'messages' => 'The selected brand doesnt exists'
                ]), 400);
            }

            $docs = [];

            foreach ($data['documents'] as $key => $doc) {
                $folderPath = "{$this->toSlugLine($brand->name)}/presentation/{$this->toSlugLine($data['type'])}/{$this->toSlugLine($data['name'])}";
                $path = $this->docStorage->putFileAs($folderPath, $doc, $this->toSlugLine($doc->getClientOriginalName()));
                $url = $this->docStorage->url($path);
                array_push($docs, $url);
            }

            $dataToCreate = [
                'name' => $data['name'],
                'brand_uuid' => $data['brand_uuid'],
                'type' => $data['type'],
                'documents' => $docs
            ];

            if (isset($data['videos'])) {
                $vids = [];
                foreach ($data['videos'] as $key => $vid) {
                    $folderPath = "{$this->toSlugLine($brand->name)}/presentation/{$this->toSlugLine($data['type'])}/{$this->toSlugLine($data['name'])}";
                    $path = $this->videoStorage->putFileAs($folderPath, $vid, $this->toSlugLine($vid->getClientOriginalName()));
                    $url = $this->videoStorage->url($path);
                    array_push($vids, $url);
                }
                $dataToCreate['videos'] = $vids;
            }


            $data = Presentation::create($dataToCreate);

            return response()->json(json_encode([
                'success' => true,
                'data' => $data,
                'message' => 'Presentation succesfully created'
            ]));
        } catch (\Exception $e) {
            return response()->json(
                json_encode(
                    [
                        'success' => false,
                        'message' => $e->getMessage()
                    ]
                ),
                500
            );
        }
    }

    public function show($uuid)
    {
        try {
            $data = Presentation::find($uuid);
            if (!$data) {
                return response()->json(json_encode([
                    'success' => false,
                    'message' => 'This presentation doesnt exists'
                ]), 404);
            }

            return response()->json(json_encode(['success' => true, 'data' => $data]));
        } catch (\Exception $e) {
            return response()->json(
                json_encode(
                    [
                        'success' => false,
                        'message' => $e->getMessage()
                    ]
                ),
                500
            );
        }
    }

    public function update(Request $request)
    {
        try {
            $data = $request->all();

            $rules = [
                'name' => 'required',
                'documents.*' => 'required|file|mimes:pdf',
                'brand_uuid' => 'required',
                'type'  => 'required|in:consumer,foods',
                'videos.*' => 'file|mimetypes:video/3gpp,video/h264,video/mp4,video/quicktime',
            ];

            $messages = [
                'name.required'  => 'The field name is required',
                'brand_uuid.required'  => 'The field brand is required',
                'documents.*.required' => 'The field document is required',
                'type.required'  => 'The field type is required',
                'type.in'        => 'The type can be only consumer or foods',
            ];

            $validation = Validator::make($data, $rules, $messages);

            if ($validation->fails()) {
                return response()->json(json_encode([
                    'success' => false,
                    'messages' => $validation->errors()
                ]), 400);
            }

            $presentation = Presentation::find($request->uuid);

            if (!$presentation) {
                return response()->json(json_encode([
                    'success' => false,
                    'message' => 'This presentation doesnt exists'
                ]), 404);
            }

            $brand = Brands::find($data['brand_uuid']);

            if (!$brand) {
                return response()->json(json_encode([
                    'success' => false,
                    'messages' => 'The selected brand doesnt exists'
                ]), 400);
            }

            if (isset($data['documents'])) {
                foreach ($presentation->documents as $key => $doc) {
                    $filePath = str_replace($this->docStorage->url('/'), '', $doc);
                    $arrayPath = explode('/', $filePath);
                    array_pop($arrayPath);
                    $folderPath = implode('/', $arrayPath);

                    if ($this->docStorage->exists($filePath)) {
                        $this->docStorage->delete($filePath);
                    }
                    $this->docStorage->deleteDirectory($folderPath);
                }

                $docs = [];

                foreach ($data['documents'] as $key => $doc) {
                    $folderPath = "{$this->toSlugLine($brand->name)}/presentation/{$this->toSlugLine($data['type'])}/{$this->toSlugLine($data['name'])}";
                    $path = $this->docStorage->putFileAs($folderPath, $doc, $this->toSlugLine($doc->getClientOriginalName()));
                    $url = $this->docStorage->url($path);
                    array_push($docs, $url);
                }

                $data['documents'] = $docs;
            } else {
                $docs = [];
                $folderPath = '';
                foreach ($presentation->documents as $key => $doc) {
                    $filePath = str_replace($this->docStorage->url('/'), '', $doc);
                    if ($presentation->name != $data['name'] || !$this->docStorage->exists($filePath) || $presentation->type != $data['type']) {
                        $newPath = str_replace($presentation->type, $data['type'], str_replace($presentation->name, $data['name'], $filePath));
                        $this->docStorage->move($filePath, $newPath);
                        array_push($docs, "{$this->docStorage->url('/')}{$newPath}");
                        $arrayPath = explode('/', $filePath);
                        array_pop($arrayPath);
                        $folderPath = implode('/', $arrayPath);
                    }
                }

                if (count($docs) > 0) {
                    $data['documents'] = $docs;
                    $this->docStorage->deleteDirectory($folderPath);
                }
            }


            if (isset($data['videos'])) {
                if (isset($presentation->videos)) {
                    foreach ($presentation->videos as $key => $vid) {
                        $filePath = str_replace($this->videoStorage->url('/'), '', $vid);
                        $arrayPath = explode('/', $filePath);
                        array_pop($arrayPath);
                        $folderPath = implode('/', $arrayPath);

                        if ($this->videoStorage->exists($filePath)) {
                            $this->videoStorage->delete($filePath);
                        }
                    }
                    $this->videoStorage->deleteDirectory($folderPath);
                }

                $vids = [];

                foreach ($data['videos'] as $key => $vid) {
                    $folderPath = "{$this->toSlugLine($brand->name)}/presentation/{$this->toSlugLine($data['type'])}/{$this->toSlugLine($data['name'])}";
                    $path = $this->videoStorage->putFileAs($folderPath, $vid, $this->toSlugLine($vid->getClientOriginalName()));
                    $url = $this->videoStorage->url($path);
                    array_push($vids, $url);
                }

                $data['videos'] = $vids;
            } else {
                if (isset($presentation->videos)) {
                    $vids = [];
                    $folderPath = '';
                    foreach ($presentation->videos as $key => $vid) {
                        $filePath = str_replace($this->videoStorage->url('/'), '', $vid);
                        if ($presentation->name != $data['name'] || !$this->videoStorage->exists($filePath) || $presentation->type != $data['type']) {
                            $newPath = str_replace($presentation->type, $data['type'], str_replace($presentation->name, $data['name'], $filePath));
                            $this->videoStorage->move($filePath, $newPath);
                            array_push($vids, "{$this->videoStorage->url('/')}{$newPath}");
                            $arrayPath = explode('/', $filePath);
                            array_pop($arrayPath);
                            $folderPath = implode('/', $arrayPath);
                        }
                    }

                    if (count($vids) > 0) {
                        $data['videos'] = $vids;
                        $this->videoStorage->deleteDirectory($folderPath);
                    }
                }
            }

            $presentation->update($data);

            return response()->json(json_encode([
                'success' => true,
                'data' => $presentation,
                'message' => 'Presentation succesfully updated'
            ]));
        } catch (\Exception $e) {
            return response()->json(
                json_encode(
                    [
                        'success' => false,
                        'message' => $e->getMessage()
                    ]
                ),
                500
            );
        }
    }

    public function destroy($uuid)
    {
        try {

            $data = Presentation::find($uuid);
            if (!$data) {
                return response()->json(json_encode([
                    'success' => false,
                    'message' => 'This presentation doesnt exists'
                ]), 404);
            }

            foreach ($data['documents'] as $key => $doc) {
                $filePath = str_replace($this->docStorage->url('/'), '', $doc);
                $arrayPath = explode('/', $filePath);
                array_pop($arrayPath);
                $folderPath = implode('/', $arrayPath);

                if ($this->docStorage->exists($filePath)) {
                    $this->docStorage->delete($filePath);
                }
            }
            $this->docStorage->deleteDirectory($folderPath);

            if (isset($data['videos'])) {

                foreach ($data['videos'] as $key => $vid) {
                    $filePath = str_replace($this->videoStorage->url('/'), '', $vid);
                    $arrayPath = explode('/', $filePath);
                    array_pop($arrayPath);
                    $folderPath = implode('/', $arrayPath);

                    if ($this->videoStorage->exists($filePath)) {
                        $this->videoStorage->delete($filePath);
                    }
                }
                $this->videoStorage->deleteDirectory($folderPath);
            }

            $data->delete();

            return response()->json(json_encode([
                'success' => true,
                'message' => 'Presentation successfully deleted'
            ]));
        } catch (\Exception $e) {
            return response()->json(
                json_encode(
                    [
                        'success' => false,
                        'message' => $e->getMessage()
                    ]
                ),
                500
            );
        }
    }

    private function toSlugLine($string)
    {

        $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'Ά', 'ά', 'Έ', 'έ', 'Ό', 'ό', 'Ώ', 'ώ', 'Ί', 'ί', 'ϊ', 'ΐ', 'Ύ', 'ύ', 'ϋ', 'ΰ', 'Ή', 'ή', 'ñ', 'Ñ');
        $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o', 'Α', 'α', 'Ε', 'ε', 'Ο', 'ο', 'Ω', 'ω', 'Ι', 'ι', 'ι', 'ι', 'Υ', 'υ', 'υ', 'υ', 'Η', 'η', 'n', 'N');
        $s = str_replace($a, $b, $string);
        $s = preg_replace('/\s/', '', $s);
        $str = str_replace('/', '-', $s);
        return strtolower($str);
    }
}