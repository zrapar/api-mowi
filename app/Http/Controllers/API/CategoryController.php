<?php

namespace App\Http\Controllers\API;

use App\Entities\Category;
use App\Entities\Products;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{

    public function getAll()
    {
        $categories = Category::with('products')->get();

        return response()->json(json_encode([
            'success' => true,
            'data' => $categories
        ]));
    }

    public function store(Request $request)
    {

        try {
            $rules = [
                'name' => 'required|unique:categories,name',
                'product_uuid' => 'required',
            ];

            $messages = [
                'name.required'  => 'The field name is required',
                'name.unique'    => 'Exist another category with the same name',
                'product_uuid.required'  => 'The field product is required',
            ];

            $validation = Validator::make($request->all(), $rules, $messages);

            if ($validation->fails()) {
                return response()->json(json_encode([
                    'success' => false,
                    'messages' => $validation->errors()
                ]), 400);
            }

            $data = $request->all();

            $product = Products::find($data['product_uuid']);

            if (!$product) {
                return response()->json(json_encode([
                    'success' => false,
                    'message' => 'This product doesnt exists'
                ]), 404);
            }

            $newCategory = Category::create($data);

            return response()->json(json_encode([
                'success' => true,
                'data' => $newCategory,
                'message' => 'Category succesfully created'
            ]));
        } catch (\Exception $e) {
            return response()->json(json_encode(
                [
                    'success' => false,
                    'message' => $e->getMessage()
                ],
                500
            ));
        }
    }

    public function show($uuid)
    {
        try {
            $category = Category::with('products')->find($uuid);
            if (!$category) {
                return response()->json(json_encode([
                    'success' => false,
                    'message' => 'This Brand doesnt exists'
                ]), 404);
            }

            return response()->json(json_encode(['success' => true, 'data' => $category]));
        } catch (\Exception $e) {
            return response()->json(
                json_encode(
                    [
                        'success' => false,
                        'message' => $e->getMessage()
                    ]
                ),
                500
            );
        }
    }

    public function update(Request $request)
    {
        try {
            $data = $request->all();

            $rules = [
                'name' => ['required'],
                'product_uuid' => 'required',
            ];

            $messages = [
                'name.required'  => 'The field name is required',
                'product_uuid.required'  => 'The field product is required',
            ];

            $validation = Validator::make($data, $rules, $messages);

            if ($validation->fails()) {
                return response()->json(json_encode([
                    'success' => false,
                    'messages' => $validation->errors()
                ]), 400);
            }

            $category = Category::find($request->uuid);

            if (!$category) {
                return response()->json(json_encode([
                    'success' => false,
                    'message' => 'This category doesnt exists'
                ]), 404);
            }

            $category->update($data);

            return response()->json(json_encode([
                'success' => true,
                'data' => $category,
                'message' => 'Brand succesfully updated'
            ]));
        } catch (\Exception $e) {
            return response()->json(json_encode(
                [
                    'success' => false,
                    'message' => $e->getMessage()
                ],
                500
            ));
        }
    }

    public function destroy($uuid)
    {
        try {

            $category = Category::find($uuid);
            if (!$category) {
                return response()->json(json_encode([
                    'success' => false,
                    'message' => 'This category doesnt exists'
                ]), 404);
            }

            $category->delete();

            return response()->json(json_encode([
                'success' => true,
                'message' => 'Category successfully deleted'
            ]));
        } catch (\Exception $e) {
            return response()->json(json_encode(
                [
                    'success' => false,
                    'message' => $e->getMessage()
                ],
                500
            ));
        }
    }
}